import 'dart:typed_data';

import 'package:example/viewer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:painter/painter.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Painter Example',
      debugShowCheckedModeBanner: false,
      home: ExamplePage(),
    );
  }
}

class ExamplePage extends StatefulWidget {
  const ExamplePage({super.key});

  @override
  _ExamplePageState createState() => _ExamplePageState();
}

class _ExamplePageState extends State<ExamplePage> {
  bool _finished = false;
  late PainterController _controller;

  PainterController _newController({String? history, bool? readOnly}) {
    PainterController controller =
        PainterController(history, readOnly: readOnly);
    controller.thickness = 5.0;
    controller.backgroundColor = Colors.green;
    return controller;
  }

  bool _hasUndo = false;
  bool _hasRedo = false;

  @override
  void initState() {
    super.initState();
    _controller = _newController();
    _controller.addListener(() {
      bool hasUndo = !_controller.isEmpty;
      bool hasRedo = !_controller.hasRemainingRedo;
      // if (_hasUndo != hasUndo || _hasRedo != hasRedo) {
        setState(() {
          _hasUndo = hasUndo;
          _hasRedo = hasRedo;
        });
      // }
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> actions;
    if (_finished) {
      actions = <Widget>[
        IconButton(
          icon: const Icon(Icons.content_copy),
          tooltip: 'New Painting',
          onPressed: () => setState(() {
            _finished = false;
            _controller = _newController();
          }),
        ),
      ];
    } else {
      actions = <Widget>[
        IconButton(
          icon: const Icon(Icons.adb_outlined),
          tooltip: 'View',
          onPressed: () async {
            var history = await getHistory();

            setState(() {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Viewer(history ?? '')),
              );
            });
          },
        ),
        IconButton(
          icon: const Icon(Icons.threesixty),
          tooltip: 'Restore',
          onPressed: () async {
            var history = await getHistory();
            setState(() {
              _finished = false;
              if (history == null) {
                _controller = _newController();
              } else {
                _controller = _newController(history: history, readOnly: false);
              }
            });
          },
        ),
        IconButton(
          icon: const Icon(
            Icons.undo,
          ),
          tooltip: 'Undo',
          onPressed: () {
                  if (_controller.isEmpty) {
                    showModalBottomSheet(
                        context: context,
                        builder: (BuildContext context) =>
                            const Text('Nothing to undo'));
                  } else {
                    _controller.undo();
                  }
                },
        ),
        IconButton(
          icon: const Icon(Icons.redo),
          tooltip: 'Redo',
          onPressed: _controller.redo,
        ),
        IconButton(
            icon: const Icon(Icons.delete),
            tooltip: 'Clear',
            onPressed: _controller.clear),
        IconButton(
            icon: const Icon(Icons.check),
            onPressed: () => _show(_controller.finish(), context)),
      ];
    }
    return Scaffold(
      appBar: AppBar(
          title: const Text('Painter Example'),
          actions: actions,
          bottom: PreferredSize(
            preferredSize: Size(MediaQuery.of(context).size.width, 30.0),
            child: DrawBar(_controller),
          )),
      body: Center(
          child: AspectRatio(aspectRatio: 1.0, child: Painter(_controller))),
    );
  }

  void _show(PictureDetails picture, BuildContext context) {
    setState(() {
      _finished = true;
    });
    _save(_controller.jsonHistory);
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('View your image'),
        ),
        body: Container(
            alignment: Alignment.center,
            child: FutureBuilder<Uint8List>(
              future: picture.toPNG(),
              builder:
                  (BuildContext context, AsyncSnapshot<Uint8List> snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.done:
                    if (snapshot.hasError) {
                      return Text('Error: ${snapshot.error}');
                    } else {
                      return Image.memory(snapshot.data!);
                    }
                  default:
                    return const FractionallySizedBox(
                      widthFactor: 0.1,
                      alignment: Alignment.center,
                      child: AspectRatio(
                          aspectRatio: 1.0, child: CircularProgressIndicator()),
                    );
                }
              },
            )),
      );
    }));
  }

  void _save(String history) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('history', history);
  }

  Future<String?> getHistory() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('history');
  }
}

class DrawBar extends StatelessWidget {
  final PainterController _controller;

  const DrawBar(this._controller, {super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Flexible(child: StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
          return Slider(
            value: _controller.thickness,
            onChanged: (double value) => setState(() {
              _controller.thickness = value;
            }),
            min: 1.0,
            max: 20.0,
            activeColor: Colors.white,
          );
        })),
        StatefulBuilder(builder: (BuildContext context, StateSetter setState) {
          return RotatedBox(
              quarterTurns: _controller.eraseMode ? 2 : 0,
              child: IconButton(
                  icon: const Icon(Icons.create),
                  tooltip:
                      '${_controller.eraseMode ? 'Disable' : 'Enable'} eraser',
                  onPressed: () {
                    setState(() {
                      _controller.eraseMode = !_controller.eraseMode;
                    });
                  }));
        }),
        ColorPickerButton(_controller, false),
        ColorPickerButton(_controller, true),
      ],
    );
  }
}

class ColorPickerButton extends StatefulWidget {
  final PainterController _controller;
  final bool _background;

  const ColorPickerButton(this._controller, this._background, {super.key});

  @override
  _ColorPickerButtonState createState() => _ColorPickerButtonState();
}

class _ColorPickerButtonState extends State<ColorPickerButton> {
  @override
  Widget build(BuildContext context) {
    return IconButton(
        icon: Icon(_iconData, color: _color),
        tooltip: widget._background
            ? 'Change background color'
            : 'Change draw color',
        onPressed: _pickColor);
  }

  void _pickColor() {
    Color pickerColor = _color;
    Navigator.of(context)
        .push(MaterialPageRoute(
            fullscreenDialog: true,
            builder: (BuildContext context) {
              return Scaffold(
                  appBar: AppBar(
                    title: const Text('Pick color'),
                  ),
                  body: Container(
                      alignment: Alignment.center,
                      child: ColorPicker(
                        pickerColor: pickerColor,
                        onColorChanged: (Color c) => pickerColor = c,
                      )));
            }))
        .then((_) {
      setState(() {
        _color = pickerColor;
      });
    });
  }

  Color get _color => widget._background
      ? widget._controller.backgroundColor
      : widget._controller.drawColor;

  IconData get _iconData =>
      widget._background ? Icons.format_color_fill : Icons.brush;

  set _color(Color color) {
    if (widget._background) {
      widget._controller.backgroundColor = color;
    } else {
      widget._controller.drawColor = color;
    }
  }
}
