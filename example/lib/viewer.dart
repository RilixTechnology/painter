import 'package:flutter/material.dart';
import 'package:painter/painter.dart';

class Viewer extends StatefulWidget {
  final String jsonHistory;

  const Viewer(this.jsonHistory, {Key? key}) : super(key: key);

  @override
  State<Viewer>createState() => _ViewerState();
}

class _ViewerState extends State<Viewer> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Viewer")),
      body: _viewer(),
    );
  }

  Widget _viewer() {
    return Container(
        width: 100,
        height: 100,
        color: Colors.blueGrey,
        child: PaintViewer(widget.jsonHistory));
  }
}
