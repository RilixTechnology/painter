import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart' hide Image;
import 'package:flutter/widgets.dart' hide Image;

import 'history.dart';
import 'path_history.dart';
import 'picture_details.dart';

class PainterController extends ChangeNotifier {
  Color _drawColor = const Color.fromARGB(255, 0, 0, 0);
  Color _backgroundColor = const Color.fromARGB(255, 255, 255, 255);
  bool _eraseMode = false;
  int compressionLevel = 4;

  double _thickness = 1.0;
  PictureDetails? _cached;
  late PathHistory _pathHistory;
  ValueGetter<Size>? widgetFinish;
  Function? onDrawStepListener;
  Function? onHistoryUpdatedListener;

  bool? _readOnly;
  History? _history;

  /// Returns true if nothing has been drawn yet.
  bool get isEmpty => _pathHistory.isEmpty;
  bool get hasRemainingRedo => _pathHistory.hasRemainingRedo;

  PainterController(String? history,
      {bool? readOnly, this.compressionLevel = 4}) {
    _setHistory(history, readOnly);
    _readOnly = readOnly;
  }

  PathHistory get pathHistory => _pathHistory;

  String get jsonHistory {
    return _serializeHistory();
  }

  double historyCanvasWidth() {
    if(_history == null) {
      return 0;
    }

    return _history!.canvasWidth;
  }

  double historyCanvasHeight() {
    if(_history == null) {
      return 0;
    }

    return _history!.canvasHeight;
  }

  void _setHistory(String? jsonHistory, bool? readOnly) {
    _history = _deserializeHistory(jsonHistory);
    _pathHistory = PathHistory(_history, readOnly: readOnly);
  }

  String _serializeHistory() {
    double width = 0;
    double height = 0;
    if (widgetFinish != null) {
      var size = widgetFinish!();
      width = size.width;
      height = size.height;
    }

    History history = History(
      paths: _pathHistory.paths,
      backgroundColor: _pathHistory.backgroundColorValue,
      canvasHeight: height,
      canvasWidth: width,
    );

    return jsonEncode(history);
  }

  // List<PathHistoryEntry>? _deserializeHistory(String? serializedHistory) {
  //   if (serializedHistory != null) {
  //     return (jsonDecode(serializedHistory) as List)
  //         .map((entry) => PathHistoryEntry.fromJson(entry))
  //         .toList();
  //   } else {
  //     return null;
  //   }
  // }
  History? _deserializeHistory(String? serializedHistory) {
    if (serializedHistory != null) {
      return History.fromJson(jsonDecode(serializedHistory));
    } else {
      return null;
    }
  }

  void setOnDrawStepListener(Function onDrawStepListener) {
    this.onDrawStepListener = onDrawStepListener;
  }

  void setOnHistoryUpdatedListener(Function onHistoryUpdatedListener) {
    this.onHistoryUpdatedListener = onHistoryUpdatedListener;
  }

  bool hasHistory() {
    return _pathHistory.historySize() != 0;
  }

  /// Returns true if the the [PainterController] is currently in erase mode,
  /// false otherwise.
  bool get eraseMode => _eraseMode;

  /// If set to true, erase mode is enabled, until this is called again with
  /// false to disable erase mode.
  set eraseMode(bool enabled) {
    if (_readOnly ?? false) return;

    _eraseMode = enabled;
    _updatePaint();
  }

  /// Retrieves the current draw color.
  Color get drawColor => _drawColor;

  /// Sets the draw color.
  set drawColor(Color color) {
    _drawColor = color;
    _updatePaint();
  }

  /// Retrieves the current background color.
  Color get backgroundColor => _backgroundColor;

  /// Updates the background color.
  set backgroundColor(Color color) {
    _backgroundColor = color;
    _updatePaint();
  }

  /// Returns the current thickness that is used for drawing.
  double get thickness => _thickness;

  /// Sets the draw thickness..
  set thickness(double t) {
    _thickness = t;
    _updatePaint();
  }

  bool get readOnly => _readOnly ?? false;

  set readOnly(bool isReadOnly) {
    _readOnly = isReadOnly;
    _updatePaint();
  }

  void _updatePaint() {
    Paint paint = Paint();
    if (_eraseMode) {
      paint.color = const Color.fromARGB(0, 255, 0, 0);
      paint.blendMode = BlendMode.clear;
    } else {
      paint.color = drawColor;
      paint.blendMode = BlendMode.srcOver;
    }
    paint.style = PaintingStyle.stroke;
    paint.strokeWidth = thickness;
    _pathHistory.currentPaint = paint;
    _pathHistory.setBackgroundColor(backgroundColor);
    notifyListeners();
  }

  /// Undoes the last drawing action (but not a background color change).
  /// If the picture is already finished, this is a no-op and does nothing.
  void undo() {
    if (_readOnly ?? false) return;

    if (!isFinished()) {
      _pathHistory.undo();
      notifyListeners();
    }
  }

  /// Redoes the last drawing action (but not a background color change).
  /// If the picture is already finished, this is a no-op and does nothing.
  void redo() {
    if (_readOnly ?? false) return;

    if (!isFinished()) {
      _pathHistory.redo();
      notifyListeners();
    }
  }

  void notifyInternalListeners() {
    notifyListeners();
  }

  /// Deletes all drawing actions, but does not affect the background.
  /// If the picture is already finished, this is a no-op and does nothing.
  void clear() {
    if (_readOnly ?? false) return;

    if (!isFinished()) {
      _pathHistory.clear();
      notifyListeners();
    }
  }

  /// Finishes drawing and returns the rendered [PictureDetails] of the drawing.
  /// The drawing is cached and on subsequent calls to this method, the cached
  /// drawing is returned.
  ///
  /// This might throw a [StateError] if this PainterController is not attached
  /// to a widget, or the associated widget's [Size.isEmpty].
  PictureDetails finish() {
    if (!isFinished()) {
      if (widgetFinish != null) {
        _cached = _render(widgetFinish!());
      } else {
        throw StateError(
            'Called finish on a PainterController that was not connected to a widget yet!');
      }
    }
    return _cached!;
  }

  PictureDetails _render(Size size) {
    if (size.isEmpty) {
      throw StateError('Tried to render a picture with an invalid size!');
    } else {
      PictureRecorder recorder = PictureRecorder();
      Canvas canvas = Canvas(recorder);
      _pathHistory.draw(canvas, size);
      return PictureDetails(
          recorder.endRecording(), size.width.floor(), size.height.floor());
    }
  }

  /// Returns true if this drawing is finished.
  ///
  /// Trying to modify a finished drawing is a no-op.
  bool isFinished() {
    return _cached != null;
  }
}
