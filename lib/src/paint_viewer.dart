import 'package:flutter/material.dart';
import 'package:painter/src/painter_controller.dart';

import 'painter_painter.dart';

class PaintViewer extends StatefulWidget {
  final String jsonHistory;

  const PaintViewer(this.jsonHistory, {Key? key}) : super(key: key);

  @override
  _PaintViewerState createState() => _PaintViewerState();
}

class _PaintViewerState extends State<PaintViewer> {
  late PainterController _controller;

  @override
  void initState() {
    _controller = PainterController(widget.jsonHistory, readOnly: true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var height = _controller.historyCanvasHeight();
    var width = _controller.historyCanvasWidth();

    return FittedBox(
      fit: BoxFit.contain,
      child: SizedBox(
        width: height,
        height: width,
        child: CustomPaint(
          willChange: true,
          painter: PainterPainter(
            _controller.pathHistory,
            repaint: _controller,
          ),
        ),
      ),
    );
  }
}
