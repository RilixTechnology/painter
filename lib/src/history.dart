import 'package:painter/src/path_history_entry.dart';

class History {
  final List<PathHistoryEntry> paths;
  final int backgroundColor;
  final double canvasWidth;
  final double canvasHeight;

  History({
    required this.paths,
    required this.backgroundColor,
    required this.canvasWidth,
    required this.canvasHeight,
  });

  factory History.fromJson(Map<String, dynamic> json) {
    return History(
      paths: (json['paths'] as List<dynamic>)
          .map((e) => PathHistoryEntry.fromJson(e as Map<String, dynamic>))
          .toList(),
      backgroundColor: json['backgroundColor'] as int,
      canvasWidth: (json['canvasWidth'] as num).toDouble(),
      canvasHeight: (json['canvasHeight'] as num).toDouble(),
    );
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'paths': paths,
      'backgroundColor': backgroundColor,
      'canvasWidth': canvasWidth,
      'canvasHeight': canvasHeight,
    };
  }
}
