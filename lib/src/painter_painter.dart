import 'package:flutter/material.dart';

import 'path_history.dart';

class PainterPainter extends CustomPainter {
  final PathHistory _path;

  PainterPainter(this._path, {Listenable? repaint}) : super(repaint: repaint);

  @override
  void paint(Canvas canvas, Size size) {
    _path.draw(canvas, size);
  }

  @override
  bool shouldRepaint(PainterPainter oldDelegate) {
    return true;
  }
}
