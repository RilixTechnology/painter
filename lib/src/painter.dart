library painter;

import 'dart:ui';

import 'package:flutter/material.dart' hide Image;
import 'package:flutter/widgets.dart' hide Image;

import 'painter_controller.dart';
import 'painter_painter.dart';
import 'picture_details.dart';

typedef PictureDetails PictureCallback();

class Painter extends StatefulWidget {
  final PainterController painterController;

  Painter(this.painterController)
      : super(key: ValueKey<PainterController>(painterController));

  @override
  _PainterState createState() => _PainterState();
}

class _PainterState extends State<Painter> {
  bool _eraseMode = false;
  bool _finished = false;
  bool _readOnly = false;

  @override
  void initState() {
    super.initState();
    widget.painterController.widgetFinish = _finish;
    _readOnly = widget.painterController.readOnly;
  }

  Size _finish() {
    setState(() {
      _finished = true;
    });
    return context.size ?? const Size(0, 0);
  }

  @override
  Widget build(BuildContext context) {
    Widget child = CustomPaint(
      willChange: true,
      painter: PainterPainter(
        widget.painterController.pathHistory,
        repaint: widget.painterController,
      ),
    );
    child = ClipRect(child: child);
    if (!_finished) {
      child = GestureDetector(
        onPanStart: _onPanStart,
        onPanUpdate: _onPanUpdate,
        onPanEnd: _onPanEnd,
        child: child,
      );
    }
    return SizedBox(
      width: double.infinity,
      height: double.infinity,
      child: child,
    );
  }

  void _onPanStart(DragStartDetails start) {
    if (_readOnly) return;

    Offset pos = (context.findRenderObject() as RenderBox)
        .globalToLocal(start.globalPosition);
    widget.painterController.pathHistory.add(pos);
    widget.painterController.notifyInternalListeners();
  }

  void _onPanUpdate(DragUpdateDetails update) {
    if (_readOnly) return;

    Offset pos = (context.findRenderObject() as RenderBox)
        .globalToLocal(update.globalPosition);
    widget.painterController.pathHistory
        .updateCurrent(pos, widget.painterController.compressionLevel);
    widget.painterController.notifyInternalListeners();
  }

  void _onPanEnd(DragEndDetails end) {
    if (_readOnly) return;

    widget.painterController.pathHistory.filterBlankHistory();
    widget.painterController.pathHistory.endCurrent();
    widget.painterController.notifyInternalListeners();
    widget.painterController.onDrawStepListener?.call();
    widget.painterController.onHistoryUpdatedListener?.call();
  }
}

