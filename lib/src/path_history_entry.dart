import 'package:flutter/material.dart' hide Image;
import 'package:flutter/widgets.dart' hide Image;

class PathHistoryEntry {
  double pathDx;
  double pathDy;

  List<Point> lineToList = [];

  int paintA;
  int paintR;
  int paintG;
  int paintB;
  int paintBlendMode;
  double paintThickness;

  PathHistoryEntry(this.pathDx, this.pathDy, this.paintA, this.paintR,
      this.paintG, this.paintB, this.paintBlendMode, this.paintThickness);

  factory PathHistoryEntry.fromJson(Map<String, dynamic> json) {
    return PathHistoryEntry(
      (json['pathDx'] as num).toDouble(),
      (json['pathDy'] as num).toDouble(),
      json['paintA'] as int,
      json['paintR'] as int,
      json['paintG'] as int,
      json['paintB'] as int,
      json['paintBlendMode'] as int,
      (json['paintThickness'] as num).toDouble(),
    )..lineToList = (json['lineToList'] as List<dynamic>)
        .map((e) => Point.fromJson(e as Map<String, dynamic>))
        .toList();
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'pathDx': pathDx,
      'pathDy': pathDy,
      'lineToList': lineToList,
      'paintA': paintA,
      'paintR': paintR,
      'paintG': paintG,
      'paintB': paintB,
      'paintBlendMode': paintBlendMode,
      'paintThickness': paintThickness,
    };
  }

  Paint extractPaint() {
    Paint paint = Paint();
    paint.color = Color.fromARGB(paintA, paintR, paintG, paintB);
    paint.blendMode = BlendMode.values[paintBlendMode];
    paint.strokeWidth = paintThickness;
    paint.style = PaintingStyle.stroke;

    return paint;
  }

  Path extractPath() {
    Path path = Path();
    path.moveTo(pathDx, pathDy);

    for (var point in lineToList) {
      path.lineTo(point.x, point.y);
    }
    return path;
  }

  MapEntry<Path, Paint> convertToPathHistoryFormat() {
    return MapEntry(extractPath(), extractPaint());
  }
}

class Point {
  double x;
  double y;

  Point(this.x, this.y);

  factory Point.fromJson(Map<String, dynamic> json) {
    return Point(
      (json['x'] as num).toDouble(),
      (json['y'] as num).toDouble(),
    );
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'x': x,
      'y': y,
    };
  }
}
