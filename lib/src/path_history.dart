import 'package:flutter/material.dart' hide Image;
import 'package:painter/src/history.dart';

import 'path_history_entry.dart';

class PathHistory {
  final List<PathHistoryEntry> _redoPaths = [];
  late List<PathHistoryEntry> _paths;
  Paint currentPaint;
  final Paint _backgroundPaint;
  bool _inDrag;
  // Function? _onDrawStepListener;

  bool _readOnly;

  bool get isEmpty => _paths.isEmpty || (_paths.length == 1 && _inDrag);
  bool get hasRemainingRedo => _redoPaths.isNotEmpty;

  List<PathHistoryEntry> get paths => _paths;

  int historySize() {
    return _paths.length;
  }

  void filterBlankHistory() {
    List<PathHistoryEntry> result = [];
    for (var path in _paths) {
      if (path.lineToList.isNotEmpty) {
        Point initialPoint = path.lineToList.first;
        for (var point in path.lineToList) {
          if (point.x != initialPoint.x || point.y != initialPoint.y) {
            result.add(path);
            break;
          }
        }
      } else {
        continue;
      }
    }
    _paths = result;
  }

  // PathHistory(List<PathHistoryEntry>? paths, {bool? readOnly})
  //     : _inDrag = false,
  //       _readOnly = false,
  //       _backgroundPaint = Paint()..blendMode = BlendMode.dstOver,
  //       currentPaint = Paint()
  //         ..color = Colors.black
  //         ..strokeWidth = 1.0
  //         ..style = PaintingStyle.fill {
  //   if (paths != null) {
  //     _paths = paths;
  //   } else {
  //     _paths = [];
  //   }
  //   // _inDrag = false;
  //   // _backgroundPaint = new Paint();
  //   _readOnly = readOnly ?? false;
  // }

  PathHistory(History? history, {bool? readOnly})
      : _inDrag = false,
        _readOnly = false,
        _backgroundPaint = Paint()..blendMode = BlendMode.dstOver,
        currentPaint = Paint()
          ..color = Colors.black
          ..strokeWidth = 1.0
          ..style = PaintingStyle.fill {
    if (history != null) {
      _paths = history.paths;
      var color = Color(history.backgroundColor);
      _backgroundPaint.color = color;

    } else {
      _paths = [];
    }
    // _inDrag = false;
    // _backgroundPaint = new Paint();
    _readOnly = readOnly ?? false;

  }
  
  // void setOnDrawStepListener(Function onDrawListener) {
  //   _onDrawStepListener = onDrawListener;
  // }

  void setBackgroundColor(Color backgroundColor) {
    _backgroundPaint.color = backgroundColor;
  }

  int get backgroundColorValue => _backgroundPaint.color.value;

  void undo() {
    if (_readOnly) return;

    if (!_inDrag) {
      if (_paths.isNotEmpty) {
        _redoPaths.add(_paths.last);
      }
      _paths.removeLast();
    }
  }

  void redo() {
    if (_redoPaths.isNotEmpty) {
      _paths.add(_redoPaths.last);
      _redoPaths.removeLast();
    }
  }

  void clear() {
    if (_readOnly) return;

    if (!_inDrag) {
      _paths.clear();
      _redoPaths.clear();
    }
  }

  void add(Offset startPoint) {
    if (!_inDrag) {
      _inDrag = true;
      Path path = Path();
      path.moveTo(startPoint.dx, startPoint.dy);
      PathHistoryEntry pathHistoryEntry = PathHistoryEntry(
          startPoint.dx,
          startPoint.dy,
          currentPaint.color.alpha,
          currentPaint.color.red,
          currentPaint.color.green,
          currentPaint.color.blue,
          currentPaint.blendMode.index,
          currentPaint.strokeWidth);
      _paths.add(pathHistoryEntry);
    }
  }

  void updateCurrent(Offset nextPoint, int compressionLevel) {
    if (_inDrag) {
      if (_paths.last.lineToList.isNotEmpty) {
        Offset prevOffset = Offset(
            _paths.last.lineToList.last.x, _paths.last.lineToList.last.y);
        if ((prevOffset - nextPoint).distance >
            _paths.last.paintThickness / compressionLevel) {
          Path path = _paths.last.extractPath();
          path.lineTo(nextPoint.dx, nextPoint.dy);
          _paths.last.lineToList.add(Point(nextPoint.dx, nextPoint.dy));
        }
      } else {
        Path path = _paths.last.extractPath();
        path.lineTo(nextPoint.dx, nextPoint.dy);
        _paths.last.lineToList.add(Point(nextPoint.dx, nextPoint.dy));
      }
    }
  }

  void endCurrent() {
    _inDrag = false;
  }

  void draw(Canvas canvas, Size size) {
    canvas.saveLayer(Offset.zero & size, Paint());
    canvas.drawRect(
        Rect.fromLTWH(0.0, 0.0, size.width, size.height), _backgroundPaint);
    for (PathHistoryEntry path in _paths) {
      MapEntry<Path, Paint> oldModel = path.convertToPathHistoryFormat();
      canvas.drawPath(oldModel.key, oldModel.value);
    }
    canvas.restore();
  }
}
